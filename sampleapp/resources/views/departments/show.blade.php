@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-6">

            <form method="GET" action="{{ route('departments.show', $departments->id) }}" class=" form-create d-flex justify-content-center flex-md-column mt-5">
                @csrf
                @method('GET')
                <h3 class="d-flex justify-content-center"> VIEW INFO DEPARTMENT</h3><br>
                <div class="mb-3">
                    <label for="department_code" class="form-label">Department Code</label>
                    <input readonly name="department_code" type="text" class="form-control" id="department_code" value="{{ $departments->department_code }}">
                    @if ($errors->has('department_code'))
                    <span class=" text-danger">{{ $errors->first('department_code') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="department_name" class="form-label">Department Name</label>
                    <input readonly name="department_name" type="text" class="form-control" id="department_name" value="{{ $departments->department_name }}">
                    @if ($errors->has('department_name'))
                    <span class=" text-danger">{{ $errors->first('department_name') }}</span>
                    @endif
                </div>
            </form>
            <a href="{{ route('departments.index') }}"><button type="submit" class="btn btn-primary">Back to home</button></a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>

    @endsection