@extends('layout')

@section('content')
<div class="container">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
    @if(session()->has('warning_success'))
    <div class="alert alert-info">
        {{ session()->get('warning_success') }}
    </div>
    @endif
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2"></div>
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="flex flex-around">
                <a href="{{ route('departments.create')}}" class="btn-create btn btn-primary">Create new</a> |
                <a href="{{ route('student.index') }}" class="btn-create btn btn-alert alert-info">Students</a>
                @csrf
                @method('GET')
                <form action="{{ route('departments.search')}}" style="width:400px; margin-top:30px; float:right; margin-bottom: 20px;">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            @csrf
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Department Code</th>
                        <th scope="col">Department Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($departments as $data)
                    <tr>
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->department_code }}</td>
                        <td>{{ $data->department_name }}</td>
                        <td style="display:flex;">
                            <a href="{{ route('departments.edit', $data->id) }}" class=" btn btn-primary" style="margin-right: 3px;">Edit</a>
                            <a href="{{ route('departments.show', $data->id) }}" class=" btn btn-warning" style="margin-right: 3px;">View</a>
                            <form method="post" action="/departments/delete/{{ $data->id }}">
                                @csrf
                                @method('delete')
                                <button type="submit" onclick="return confirm('Are you sure you want to delete this item?')" ; class=" btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $departments->links()}}
        </div>
        <div class=" col-md-2 col-sm-2 col-xs-2">
        </div>
    </div>
</div>
@endsection