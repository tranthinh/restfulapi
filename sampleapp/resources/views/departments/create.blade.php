@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            @method('POST')
            <form method="POST" action="{{ route('departments.store') }}" class=" form-create d-flex justify-content-center flex-md-column mt-5">
                @csrf
                <h3 class="d-flex justify-content-center"> ADD NEW DEPARTMENT</h3><br>
                <div class="mb-3">
                    <div class="mb-3">
                        <label for="department_code" class="form-label">Department Code</label>
                        <input name="department_code" type="text" class="form-control" id="Age">
                        @if ($errors->has('department_code'))
                        <span class="text-danger">{{ $errors->first('department_code') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="department_name" class="form-label">Department Name</label>
                        <input name="department_name" type="text" class="form-control" id="fullname">
                        @if ($errors->has('department_name'))
                        <span class="text-danger">{{ $errors->first('department_name') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>

    @endsection