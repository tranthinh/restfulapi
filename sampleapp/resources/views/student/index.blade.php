@extends('layout')

@section('content')
<div class="container">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
    @if(session()->has('warning_success'))
    <div class="alert alert-info">
        {{ session()->get('warning_success') }}
    </div>
    @endif
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2"></div>
        <div class="col-md-8 col-sm-8 col-xs-8">
            <a href="{{ route('student.create')}}" class="btn-create btn btn-primary">Create new</a> |
            <a href="{{ route('departments.index')}}" class="alert alert-info">Departments</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Fullname</th>
                        <th scope="col">Age</th>
                        <th scope="col">Classes</th>
                        <th scope="col">Course</th>
                        <th scope="col">Department Name</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($students as $data)
                    <tr>
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->fullname }}</td>
                        <td>{{ $data->age }}</td>
                        <td>{{ $data->classes }}</td>
                        <td>{{ $data->course }}</td>
                        <td>{{ $data->department->department_name }}</td>
                        <td>
                            <a href="{{ route('student.show', $data->id) }}" class=" btn btn-warning">View</a>
                            <a href="{{ route('student.edit', $data->id) }}" class=" btn btn-primary">Edit</a>

                            @csrf
                            @method('DELETE')
                            <a href="{{ route('student.delete', $data->id) }}" class=" btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?')" ;>Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $students->links()}}
        </div>
        <div class=" col-md-2 col-sm-2 col-xs-2">
        </div>
    </div>
</div>

@endsection