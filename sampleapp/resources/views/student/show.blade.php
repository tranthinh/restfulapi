@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-6">

            <form method="GET" action="{{ route('student.show', $student) }}" class=" form-create d-flex justify-content-center flex-md-column mt-5">
                @csrf
                @method('GET')
                <h3 class="d-flex justify-content-center"> VIEW INFO STUDENT</h3><br>
                <div class="mb-3">
                    <label for="fullname" class="form-label">Fullname</label>
                    <input readonly name="fullname" type="text" class="form-control" id="fullname" value="{{ $student->fullname }}">
                    @if ($errors->has('fullname'))
                    <span class=" text-danger">{{ $errors->first('fullname') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Age" class="form-label">Age</label>
                    <input readonly name="age" type="text" class="form-control" id="Age" value="{{ $student->age }}">
                    @if ($errors->has('age'))
                    <span class=" text-danger">{{ $errors->first('age') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Classes" class="form-label">Classes</label>
                    <input readonly name="classes" type="text" class="form-control" id="Classes" value="{{ $student->classes }}">
                    @if ($errors->has('classes'))
                    <span class="text-danger">{{ $errors->first('classes') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Course" class="form-label">Course</label>
                    <input readonly name="course" type="text" class="form-control" id="Course" value="{{ $student->course }}">
                    @if ($errors->has('course'))
                    <span class="text-danger">{{ $errors->first('course') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Department" class="form-label">Department</label>
                    <input readonly name="department_id" type="text" class="form-control" id="Course" value="{{ $student->department->department_name }}">
                    @if ($errors->has('department_id'))
                    <span class="text-danger">{{ $errors->first('department_id') }}</span>
                    @endif
                </div>
            </form>
            <a href="{{ route('student.index') }}"><button type="submit" class="btn btn-primary">Back to home</button></a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>

    @endsection