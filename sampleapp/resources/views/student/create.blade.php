@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-6">

            <form method="POST" action="{{ route('student.store') }}" class=" form-create d-flex justify-content-center flex-md-column mt-5">
                @csrf
                <h3 class="d-flex justify-content-center"> ADD NEW STUDENT</h3><br>
                <div class="mb-3">
                    <label for="fullname" class="form-label">Fullname</label>
                    <input name="fullname" type="text" class="form-control" id="fullname">
                    @if ($errors->has('fullname'))
                    <span class="text-danger">{{ $errors->first('fullname') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Age" class="form-label">Age</label>
                    <input name="age" type="text" class="form-control" id="Age">
                    @if ($errors->has('age'))
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Classes" class="form-label">Classes</label>
                    <input name="classes" type="text" class="form-control" id="Classes">
                    @if ($errors->has('classes'))
                    <span class="text-danger">{{ $errors->first('classes') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Course" class="form-label">Course</label>
                    <input name="course" type="text" class="form-control" id="Course">
                    @if ($errors->has('course'))
                    <span class="text-danger">{{ $errors->first('course') }}</span>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="Department" class="form-label">Department</label>
                    <select name="department_id" class="form-select form-control" aria-label="Default select department">
                        @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('department_id'))
                    <span class="text-danger">{{ $errors->first('department_id') }}</span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>

    @endsection