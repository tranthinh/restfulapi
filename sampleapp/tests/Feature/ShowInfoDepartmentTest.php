<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Department;
use App\Models\User;

class ShowInfoDepartmentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function getRouteShowInfoDepartment($id)
    {
        return route('departments.show', $id);
    }


    /** @test */
    public function unauthenticated_user_cannot_view_department_info()
    {
        $department = Department::factory()->create();
        $response = $this->get($this->getRouteShowInfoDepartment($department->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_cannot_view_department_info_with_invalid_id()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->get($this->getRouteShowInfoDepartment($departments->id == null));
        $response->assertStatus(404);
    }

    /** @test */
    public function authenticated_user_can_view_department_info_with_valid_id()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->get($this->getRouteShowInfoDepartment($departments->id));
        $response->assertStatus(200);
        $response->assertViewIs('departments.show');
    }
}
