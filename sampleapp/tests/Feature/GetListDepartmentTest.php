<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Response;

class GetListDepartmentTest extends TestCase
{
    use RefreshDatabase;

    public function GetListRouteDepartment()
    {
        return route('departments.index');
    }
    /** @test */
    public function authenticate_user_can_get_all_departments()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->get($this->GetListRouteDepartment());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('departments.index');
        $response->assertSee($departments->department_name);
    }

    /** @test */
    public function authenticate_user_cannot_get_all_departments()
    {
        $response = $this->get($this->GetListRouteDepartment());
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticate_user_can_get_all_departments_with_search()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->get($this->GetListRouteDepartment() . '?search=' . $departments->department_name);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('departments.index');
        $response->assertSee($departments->department_name);
    }
}
