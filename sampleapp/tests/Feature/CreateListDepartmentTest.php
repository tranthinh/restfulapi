<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Department;
use App\Models\User;



use Illuminate\Http\Response;

class CreateListDepartmentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function GetCreateRouteDepartment()
    {
        return route('departments.store');
    }

    public function GetViewCreateDepartmentsRoute()
    {
        return route('departments.create');
    }
    /** @test */
    //*When user is logged in
    public function authentication_user_can_create_departments()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->make()->toArray();
        $response = $this->post($this->GetCreateRouteDepartment(), $departments);
        $response->assertStatus(302);
        $this->assertDatabaseHas('departments', $departments);
        $response->assertRedirect(route('departments.index'));
    }
    /** @test */
    //*When user is not logged in
    public function authentication_user_cannot_create_departments()
    {
        $departments = Department::factory()->make()->toArray();
        $response = $this->post($this->GetCreateRouteDepartment(), $departments);
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }
    /** @test */
    public function authentication_user_cannot_create_when_fill_blank()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->make(['department_name' => null, 'department_code' => null])->toArray();
        $response = $this->post($this->GetCreateRouteDepartment(), $departments);
        $response->assertSessionHasErrors(['department_name', 'department_code']);
    }
    /** @test */
    public function authentication_user_can_view_create_department_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get(
            $this->GetViewCreateDepartmentsRoute()
        );
        $response->assertViewIs('departments.create');
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test */
    public function authentication_user_can_see_input_request_when_validate_form()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->make(['department_name' => null, 'department_code' => null])->toArray();
        $response = $this->from($this->GetViewCreateDepartmentsRoute())->post($this->GetCreateRouteDepartment(), $departments);
        $response->assertRedirect($this->GetViewCreateDepartmentsRoute());
        $response->assertSessionHasErrors(['department_name', 'department_code']);
        $this->assertDatabaseMissing('departments', $departments);
    }

    /** @test */
    public function authentication_user_cannot_see_create_form_view()
    {
        $departments = Department::factory()->make(['department_name' => null, 'department_code' => null])->toArray();
        $response = $this->from($this->GetViewCreateDepartmentsRoute())->post($this->GetCreateRouteDepartment(), $departments);
        $response->assertRedirect('login');
    }
}
