<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Department;
use App\Models\User;

class SearchDepartmentTest extends TestCase
{
    use RefreshDatabase;
    /** @test*/
    public function authenticate_user_can_search_name_departments()
    {
        $this->actingAs(User::factory()->create());
        $department = Department::factory()->create();
        $response = $this->get('/departments/search?search=' . $department->name . '&submit=Search');
        $response->assertStatus(200);
        $response->assertSee($department->name);
    }

    /** @test*/
    public function authenticate_user_can_search_code_departments()
    {
        $this->actingAs(User::factory()->create());
        $department = Department::factory()->create();
        $response = $this->get('/departments/search?search=' . $department->code . '&submit=Search');
        $response->assertStatus(200);
        $response->assertSee($department->code);
    }

    /** @test*/
    public function authenticate_user_cannot_search_name_departments()
    {
        $department = Department::factory()->create();
        $response = $this->get('/departments/search?search=' . $department->name . '&submit=Search');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test*/
    public function authenticate_user_cannot_search_code_departments()
    {
        $department = Department::factory()->create();
        $response = $this->get('/departments/search?search=' . $department->code . '&submit=Search');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
}
