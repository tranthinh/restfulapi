<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Department;
use App\Models\User;

class DeleteDepartmentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function getRouteDeleteDepartment($id)
    {
        return route('departments.destroy', $id);
    }

    /** @test */
    public function authenticate_user_can_delete_department()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->delete($this->getRouteDeleteDepartment($departments->id));
        $response->assertStatus(302);
        $response->assertRedirect('/departments');
    }

    /** @test */
    public function authenticate_user_cannot_delete_department()
    {
        $departments = Department::factory()->create();
        $response = $this->delete($this->getRouteDeleteDepartment($departments->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticate_cannot_delete_department_with_id_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $department = Department::factory()->create();
        $response = $this->delete($this->getRouteDeleteDepartment($department->id == 9999));
        $response->assertStatus(404);
    }
}
