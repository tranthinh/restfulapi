<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use App\Models\Department;

class UpdateListDepartmentTest extends TestCase
{
    use RefreshDatabase;


    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function getViewRouteUpdateDepartment($id)
    {
        return route('departments.edit', $id);
    }

    public function getUpdateRouteDepartment($id)
    {
        return route('departments.update', $id);
    }


    /** @test */
    public function authenticate_user_can_view_update_form()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->get($this->getViewRouteUpdateDepartment($departments->id));
        $response->assertViewIs("departments.edit");
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticate_user_cannot_view_update_form_department()
    {
        $departments = Department::factory()->create();
        $response = $this->get($this->getViewRouteUpdateDepartment($departments->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_cannot_update_department()
    {
        $departments = Department::factory()->create();
        $response = $this->put($this->getUpdateRouteDepartment($departments->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_update_department()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->put($this->getUpdateRouteDepartment($departments->id));
        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    /** @test */
    public function authentication_user_cannot_update_when_fill_blank()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->put($this->getUpdateRouteDepartment($departments->id), [
            'department_code' => '',
            'depảtment_name' => '',
        ]);
        $response->assertSessionHasErrors(['department_name', 'department_code']);
    }

    /** @test */
    public function authentication_user_cannot_update_when_fill_is_unique()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $departments2 = Department::factory()->create();
        $response = $this->put($this->getUpdateRouteDepartment($departments->id), [
            'department_code' => $departments2->department_code,
            'department_name' => $departments2->department_name,
        ]);
        $response->assertSessionHasErrors(['department_code', 'department_name']);
    }

    /** @test */
    public function authentication_user_can_see_input_request_when_validate_form()
    {
        $this->actingAs(User::factory()->create());
        $departments = Department::factory()->create();
        $response = $this->from('departments/edit/' . $departments->id)->put($this->getUpdateRouteDepartment($departments->id), [
            'department_code' => "",
            'department_name' => "",
        ]);
        $response->assertRedirect('/departments/edit/' . $departments->id);
        $response->assertSessionHasErrors(['department_code', 'department_name']);
    }
}
