<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/departments');
});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post');
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post');
Route::get('dashboard', [AuthController::class, 'dashboard']);
Route::get('logout', [AuthController::class, 'logout'])->name('logout');


//Student
Route::get('student', [StudentController::class, 'index'])->name('student.index')->middleware('auth');
Route::get('student/create', [StudentController::class, 'create'])->name('student.create');
Route::post('student/store', [StudentController::class, 'store'])->name('student.store')->middleware('auth');
Route::get('student/edit/{id}', [StudentController::class, 'edit'])->name('student.edit');
Route::put('student/update/{id}', [StudentController::class, 'update'])->name('student.update')->middleware('auth');
Route::get('student/delete/{id}', [StudentController::class, 'destroy'])->name('student.delete');
Route::get('student/show/{id}', [StudentController::class, 'show'])->name('student.show');

// Department
Route::get('departments', [DepartmentController::class, 'index'])->name('departments.index')->middleware('auth');
Route::post('/departments/store', [DepartmentController::class, 'store'])->name('departments.store')->middleware('auth');
Route::get('/departments/create', [DepartmentController::class, 'create'])->name('departments.create');
Route::get('/departments/edit/{id}', [DepartmentController::class, 'edit'])->name('departments.edit')->middleware('auth');
Route::put('/departments/update/{id}', [DepartmentController::class, 'update'])->name('departments.update')->middleware('auth');
Route::delete('departments/delete/{id}', [DepartmentController::class, 'destroy'])->name('departments.destroy')->middleware('auth');
Route::get('/departments/show/{id}', [DepartmentController::class, 'show'])->name('departments.show')->middleware('auth');
Route::get('/departments/search', [DepartmentController::class, 'search'])->name('departments.search')->middleware('auth');
