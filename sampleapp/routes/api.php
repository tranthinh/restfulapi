<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiStudentController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::apiResource("students", StudentController::class);
    Route::apiResource("departments", DepartmentController::class);
    Route::get("logout", [AuthController::class, "logout"]);
});


Route::resource('/students', ApiStudentController::class);
Route::resource('/departments', DepartmentController::class);
Route::fallback(function () {
    return response()->json([
        'status' => 'failed',
        'message' => 'Page Not Found!!!!!'
    ], 404);
}); */


