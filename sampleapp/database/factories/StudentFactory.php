<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Student;
use Faker\Generator as Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Student>
 */
class StudentFactory extends Factory
{
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition()
    {
        return [
            'fullname' => $this->faker->name(),
            'age' => $this->faker->numberBetween(18, 30),
            'classes' => $this->faker->text(),
            'course' => $this->faker->text(),
            'department_id' => $this->faker->numberBetween(1, 3),
        ];
    }
}
