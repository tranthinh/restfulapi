<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*         \App\Models\Student::factory(2)->create(); */
       /*  DB::table('departments')->insert([
            'department_code' => 'C09913',
            'department_name' => 'Giao duc quan su'
        ]); */
        Department::factory(1)->create();
    }
}
