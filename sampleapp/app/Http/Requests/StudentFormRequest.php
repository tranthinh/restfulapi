<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required|string|min:5|max:255',
            'age' => 'required|integer|min:18|max:100',
            'classes' => 'required|string|min:7|max:35',
            'course' => 'required|string|min:3|max:50',
            'department_id' => 'required|integer|exists:departments,id'
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'The :attribute field can not be blank value',
            'age.required' => 'The :attribute field can not be blank value',
            'classes.required' => 'The :attribute field can not be blank value',
            'course.required' => 'The :attribute field can not be blank value',
            'fullname.string' => 'The :attribute field must be string',
            'age.integer' => 'The :attribute field must be integer',
            'classes.string' => 'The :attribute field must be string',
            'course.string' => 'The :attribute field must be string',
            'fullname.min' => 'The :attribute field must be at least 5 characters',
            'age.min' => 'The :attribute field must be at least 18 characters',
            'classes.min' => 'The :attribute field must be at least 7 characters',
            'course.min' => 'The :attribute field must be at least 3 characters',
            'fullname.max' => 'The :attribute field must be at most 255 characters',
            'age.max' => 'The :attribute field must be at most 100 characters',
            'classes.max' => 'The :attribute field must be at most 35 characters',
            'course.max' => 'The :attribute field must be at most 50 characters',
            'department_id.integer' => 'The :attribute field must be integer',
        ];
    }

    public function attributes()
    {
        return [
            'fullname' => 'Fullname',
            'age' => 'Age',
            'classes' => 'Classes',
            'course' => 'Course',
            'department_id' => 'Department'
        ];
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function ($validator) {
            if ($this->department_id == 0) {
                $validator->errors()->add('department_id', 'The :attribute field can not be blank value');
            }
        });

        return $validator;
    }

    
}
