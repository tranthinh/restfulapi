<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        #cho phep su dung
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_code' => 'string|unique:departments|min:5|max:15',
            'department_name' => 'required|string|unique:departments|min:5',
        ];
    }
    public function messages()
    {
        return [
            'department_code.required' => 'The :attribute field can not be blank value',
            'department_name.required' => 'The :attribute field can not be blank value',
            'department_code.unique' => 'The :attribute field must be unique',
            'department_name.unique' => 'The :attribute field must be unique',
            'department_code.min' => 'The :attribute field must be at least 5 characters',
            'department_name.min' => 'The :attribute field must be at least 5 characters',
            'department_code.max' => 'The :attribute field must be at most 15 characters',

        ];
    }
    
    public function attributes(){
        return [
            'department_code' => 'Department Code',
            'department_name' => 'Department Name',
        ];
    } 
    
}
