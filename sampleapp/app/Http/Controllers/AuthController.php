<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;
use App\Http\Requests\AuthFormRequest;
use App\Http\Requests\RegisterRequest;


class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registration()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(AuthFormRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            Auth::user()->name;
            return redirect()->intended('/student')
                ->withSuccess('You have Successfully loggedin');
        }
        return redirect("login")->with('failed', 'Invalid Email or Password!');
    }
    public function postRegistration(RegisterRequest $request)
    {
        $request['password'] = Hash::make($request['password']);
        $user = User::create($request->all());
        return redirect('login')->with('Success', 'You have Successfully registered');
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function dashboard()
    {
        if (Auth::check()) {
            return view('student.index');
        } else {
            return redirect("login")->with('failed', 'You are not logged in!');
        }
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
