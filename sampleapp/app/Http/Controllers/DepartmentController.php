<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Http\Requests\DepartmentFormRequest;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    protected $departments;

    public function __construct(Department $departments)
    {
        $this->departments = $departments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = $this->departments->latest('id')->paginate(3);
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentFormRequest $request)
    {

        $this->departments->create($request->all());
        return redirect()->route('departments.index')->with('success', 'Department created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departments = $this->departments->find($id);
        return view('departments.show', compact('departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = $this->departments->find($id);
        return view('departments.edit', compact('departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentFormRequest $request, $id)
    {
        $this->departments->find($id)->update($request->all());
        return redirect()->route('departments.index')->with('success', 'Department updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $this->departments->find($id)->delete();
        return redirect()->route('departments.index')->with('success', 'Department deleted successfully');
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $departments = $this->departments->where('department_name', 'like', '%' . $search . '%')->orWhere('department_code', 'like', '%' . $search . '%')->paginate(3);
        
        return view('departments.index', compact('departments'));
    }
}
