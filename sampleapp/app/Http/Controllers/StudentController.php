<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Department;
use App\Http\Requests\StudentFormRequest;
use App\Http\Controllers\Controller;
use App\Repositories\RepositoryInterface;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    protected $studentSer;

    public function __construct(RepositoryInterface $studentSer)
    {
        $this->studentSer = $studentSer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*  return view("student.index", ["students" => $this->studentSer->paginate()]); */
        $students = $this->studentSer->paginate();
        return view('student.index', compact('students'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('student.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentFormRequest $request)
    {

        $this->studentSer->create($request->all());
        return redirect()->route('student.index')->with('success', 'Student created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $student = $this->studentSer->find($id);
        return view('student.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = $this->studentSer->find($id);
        $departments = Department::all(); #get all departments show in combobox
        return view('student.edit', compact('student', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentFormRequest $request, $id)
    {
        $this->studentSer->update($id, $request->all());
        return redirect()->route('student.index')->with('success', 'Student updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->studentSer->delete($id);
        return redirect()->route('student.index')->with('success', 'Student has id ' . $id . ' deleted successfully ');
    }
}
