<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Http\Requests\DepartmentFormRequest;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::all();
        return response([
            'status' => 'success',
            'message' => 'Department retrieved successfully',
            'department' => $department
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentFormRequest $request)
    {
        $department = Department::create($request->all());
        return response([
            'status' => 'success',
            'message' => 'Department created successfully',
            'department' => $department
        ], 201);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        if (!$department) {
            return response([
                'status' => 'failed',
                'message' => 'Department not found to show !!!',
            ], 404);
        } else {
            return response([
                'status' => 'success',
                'message' => 'Department retrieved successfully',
                'department' => $department
            ], 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentFormRequest $request, $id)
    {
        $department = Department::find($id);
        if (!$department) {
            return response([
                'status' => 'failed',
                'message' => 'Department not found to update !!!',
            ], 404);
        } else {
            $department->update($request->all());
            return response([
                'status' => 'success',
                'message' => 'Department updated successfully',
                'department' => $department
            ], 200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        if (!$department) {
            return response([
                'status' => 'failed',
                'message' => 'Department not found to delete !!!',
            ], 404);
        } else {
            $department->delete();
            return response([
                'status' => 'success',
                'message' => 'Department deleted successfully',
            ], 200);
        }
    }
}
