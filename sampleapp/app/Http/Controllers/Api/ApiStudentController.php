<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Http\Requests\StudentFormRequest;

class ApiStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::all();
        return response([
            'status' => 'success',
            'message' => 'Student retrieved successfully',
            'student' => $student
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //show form
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentFormRequest $request)
    {
        $student = Student::create($request->all());
        return response([
            'status' => 'success',
            'message' => 'Student created successfully',
            'student' => $student
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        if (!$student) {
            return response([
                'status' => 'failed',
                'message' => 'Student not found to show !!!',
            ], 404);
        } else {
            return response([
                'status' => 'success',
                'message' => 'Student retrieved successfully',
                'student' => $student
            ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //show form edit
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentFormRequest $request, $id)
    {
        $student = Student::find($id);
        if (!$student) {
            return response([
                'status' => 'failed',
                'message' => 'Student not found to update !!!',
            ], 404);
        } else {
            $student->update($request->all());
            return response([
                'status' => 'success',
                'message' => 'Student updated successfully',
                'student' => $student
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        if (!$student) {
            return response([
                'status' => 'failed',
                'message' => 'Student not found to delete !!!',
            ], 404);
        } else {
            $student->delete();
            return response([
                'status' => 'success',
                'message' => 'Student deleted successfully',
            ], 200);
        }
    }
}
