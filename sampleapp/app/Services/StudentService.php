<?php

namespace App\Services;

use App\Services\ServiceInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentService implements ServiceInterface
{
    protected $repo;

    public function __construct(ServiceInterface $request)
    {
        $this->repo = $request;
    }


    public function getAll()
    {
        return $this->repo->getAll();
    }

    public function find($id)
    {
        return $this->repo->find($id);
    }

    public function create($attributes = [])
    {
        return $this->repo->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        return $this->repo->update($id, $attributes);
    }

    public function delete($id)
    {
        return $this->repo->delete($id);
    }

    public function paginate()
    {
        return $this->repo->paginate();
    }
}
