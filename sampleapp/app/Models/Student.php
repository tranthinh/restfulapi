<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Database\Factories\StudentFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected static function newFactory()
    {
        return StudentFactory::new();
    }
    
    protected $table = 'students';

    protected $fillable = [
        "id",
        "fullname",
        "age",
        "classes",
        "course",
        "department_id",
    ];
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }
}
